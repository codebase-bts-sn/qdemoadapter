
# QDemoAdapter

Exemple d'application console *Qt* illustrant le *design pattern*  ***Adapter***.

L'application permet d'accéder **de manière unifiée** à 2 capteurs de températures disposant d'API différentes fournies sous forme de librairies statiques (-> `libfoosensor.a` et `libbarsensor.a`).

Le 1er type de capteur (-> `FooSensor`) dispose d'une méthode d'API `getTemperature()` qui retourne une température en °C.

Le 2ème type de capteur (-> BarSensor) dispose d'une méthode d'API `readTemp()` qui retourne une température en °F.

L'objectif de l'application est d'accéder à ces 2 types de capteur à l'aide d'une méthode unique `readTemperature()` qui retourne une température en °C en mettant en oeuvre le *design pattern* ***Adapter***.

![alt text](img/adapter-solution.svg  "Design pattern Adapter")

![alt text](img/qdemoadapter.png  "Qt App screenshot")

NOTE : 

* Ce code source a été développé sous _Windows_. Adapter les fichier `.pro` pour les compiler sur une autre plateforme.

* Une erreur de compilation peut se produire lors de la 1ère génération de l'exécutable. Dans ce cas, relancer un 2ème *Build*.