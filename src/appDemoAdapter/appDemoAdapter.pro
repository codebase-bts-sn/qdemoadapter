QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        baradapter.cpp \
        capteurtemperature.cpp \
        fooadapter.cpp \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    baradapter.h \
    capteurtemperature.h \
    fooadapter.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../foosensor/release/ -lfoosensor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../foosensor/debug/ -lfoosensor

INCLUDEPATH += $$PWD/../foosensor
DEPENDPATH += $$PWD/../foosensor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../foosensor/release/libfoosensor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../foosensor/debug/libfoosensor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../foosensor/release/foosensor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../foosensor/debug/foosensor.lib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../barsensor/release/ -lbarsensor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../barsensor/debug/ -lbarsensor

INCLUDEPATH += $$PWD/../barsensor
DEPENDPATH += $$PWD/../barsensor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../barsensor/release/libbarsensor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../barsensor/debug/libbarsensor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../barsensor/release/barsensor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../barsensor/debug/barsensor.lib
