#ifndef CAPTEURTEMPERATURE_H
#define CAPTEURTEMPERATURE_H


class CapteurTemperature
{
public:
    CapteurTemperature();
    virtual double readTemperature() = 0;
};

#endif // CAPTEURTEMPERATURE_H
