#include <iostream>

using namespace std;

#include "fooadapter.h"

FooAdapter::FooAdapter()
{
}

double FooAdapter::readTemperature()
{
    cout << "FooAdapter::readTemperature() -> ";
    return m_capteur.getTemperature();
}
