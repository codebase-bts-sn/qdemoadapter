#ifndef BARADAPTER_H
#define BARADAPTER_H

#include "capteurtemperature.h"
#include "../barsensor/barsensor.h"

class BarAdapter : public CapteurTemperature
{
public:
    BarAdapter();
    double readTemperature() override;
private:
    BarSensor m_capteur;
};

#endif // BARADAPTER_H
