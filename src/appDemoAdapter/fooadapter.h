#ifndef FOOADAPTER_H
#define FOOADAPTER_H

#include "capteurtemperature.h"
#include "../foosensor/foosensor.h"

class FooAdapter : public CapteurTemperature
{
public:
    FooAdapter();
    double readTemperature() override;
private:
    FooSensor m_capteur;
};

#endif // FOOADAPTER_H
