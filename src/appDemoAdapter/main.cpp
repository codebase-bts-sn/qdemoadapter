#include <iostream>

using namespace std;

#include <QCoreApplication>
#include <QVector>

#include "fooadapter.h"
#include "baradapter.h"

int main(int argc, char *argv[])
{
    double total = 0.0;
    double moyenne = 0.0;
    int i;

    QCoreApplication a(argc, argv);

    vector<CapteurTemperature *> capteurs;

    cout << ">>> On instancie 5 capteurs de chaque type" << endl;

    for(i = 0; i < 5; i++)
        capteurs.push_back(new FooAdapter());

    for(i = 0; i < 5; i++)
        capteurs.push_back(new BarAdapter());

    cout << ">> On lit les valeurs des 10 capteurs de facon unifiee" << endl;
    cout << ">> avec la methode readTemperature() de chaque Adapter" << endl;
    vector<CapteurTemperature *>::iterator it;

    for(it = capteurs.begin(); it != capteurs.end(); it++) {
        total += (*it)->readTemperature();
    }

    cout << ">> On affiche la moyenne"  << endl;

    moyenne = total / 10;

    cout << "moyenne des 10 temperatures = " << total << " / 10 = " << moyenne << endl;

    return a.exec();
}
