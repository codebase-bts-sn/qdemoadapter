#include <iostream>

using namespace std;

#include "baradapter.h"

BarAdapter::BarAdapter()
{

}

double BarAdapter::readTemperature()
{
    cout << "BarAdapter::readTemperature() -> ";
    return (m_capteur.readTemp() - 32) * 5/9;
}
