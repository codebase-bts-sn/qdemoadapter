#include <iostream>

#include "foosensor.h"

using namespace std;

FooSensor::FooSensor()
{
    cout << "Instanciation FooSensor" << endl;
}

double FooSensor::getTemperature() {
    cout << "FooSensor::getTemperature()" << endl;
    return 19; // 19°C
}
