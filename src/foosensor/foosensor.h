#ifndef FOOSENSOR_H
#define FOOSENSOR_H

class FooSensor
{
public:
    explicit FooSensor();
    double getTemperature();
};

#endif // FOOSENSOR_H
