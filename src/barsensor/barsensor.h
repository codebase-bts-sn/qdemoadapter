#ifndef BARSENSOR_H
#define BARSENSOR_H

class BarSensor
{
public:
    BarSensor();
    double readTemp();
};

#endif // BARSENSOR_H
