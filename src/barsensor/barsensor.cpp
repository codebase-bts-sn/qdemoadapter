#include <iostream>

using namespace std;

#include "barsensor.h"

BarSensor::BarSensor()
{
    cout << "Instanciation BarSensor" << endl;
}

double BarSensor::readTemp()
{
    cout << "BarSensor::readTemp" << endl;
    return 66.2; // 66.2°F = 19°C
}
